﻿using System;

namespace Restaurant.Drinks.Behaviors
{
    class Cooling : CookingBehavior
    {
        public String cook()
        {
            return("Preparation method : cool ");
        }
    }
}
