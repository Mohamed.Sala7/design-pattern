﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Drinks.Behaviors
{
    interface CookingBehavior
    {
        String cook();
    }
}
