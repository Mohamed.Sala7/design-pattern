﻿using System;

namespace Restaurant.Drinks.Behaviors
{
    class Heat : CookingBehavior
    {
        public String cook()
        {
            return(" Preparation method : heat ");
        }
    }
}
