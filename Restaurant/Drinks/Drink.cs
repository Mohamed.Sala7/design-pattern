﻿using System;
using Restaurant.Drinks.Behaviors;

namespace Restaurant.Drinks
{
    abstract class Drink
    {
        protected CookingBehavior cookingBehavior;

        protected String description = "";
        protected String preparation = "";

        public virtual String getDescription()
        {
            return description;
        }
        public virtual String getPreparation()
        {
            return preparation;
        }

        public abstract double getCost();

        public void setCookingBehavior(CookingBehavior cb)
        {
            this.cookingBehavior = cb;
        }
        public void performCookingBehavior()
        {
            this.preparation = this.cookingBehavior.cook();
        }

    }
}
