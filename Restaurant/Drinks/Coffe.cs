﻿using System;
using Restaurant.Drinks.Behaviors;

namespace Restaurant.Drinks
{
    class Coffe : Drink
    {
        private double cost = 20;

        public Coffe(String name)
        {
            description = name;
            cookingBehavior = new Heat();
        }
        public override double getCost()
        {
            return cost;
        }
    }
}
