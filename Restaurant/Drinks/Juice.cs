﻿using System;
using Restaurant.Drinks.Behaviors;

namespace Restaurant.Drinks
{
    class Juice : Drink
    {
        private double cost = 10;

        public Juice(String name)
        {
            description = name;
            cookingBehavior = new Cooling();
        }
        public override double getCost()
        {
            return cost;
        }
    }
}
