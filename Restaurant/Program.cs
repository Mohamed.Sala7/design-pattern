﻿using System;
using Restaurant.Drinks;
using Restaurant.Extras;
using Restaurant.Observers;
using Restaurant.Drinks.Behaviors;

namespace Restaurant
{
    class Program
    {
        static void Main(string[] args)
        {
            Drink espresso = new Coffe("Espresso");
            Drink orangeJuice = new Juice("Orange ");
            Drink lemonJuice = new Juice("Lemon ");

            // ------------------ The Startegy Pattern ------------------ //
            espresso.performCookingBehavior();
            orangeJuice.performCookingBehavior();
            lemonJuice.performCookingBehavior();


            Console.WriteLine("*************************************************************");
            // ------------------ The Decorator Pattern ------------------ //
            Console.WriteLine(espresso.getDescription() + ": $" + espresso.getCost());

            orangeJuice = new Milk(orangeJuice);
            orangeJuice = new Mocha(orangeJuice);
            Console.WriteLine(orangeJuice.getDescription() + ": $" + orangeJuice.getCost());


            Console.WriteLine("*************************************************************");
            // ------------------ The Observer Pattern ------------------ //
            Order orderData = new Order();

            Accountant accountant = new Accountant(orderData);
            Accountant generalAccountant = new Accountant(orderData);
            Kitchen kitchen =  Kitchen.getInstance(orderData); // ----------- Singleton -------------

            orderData.setOrder(espresso);
            orderData.setOrder(orangeJuice);

            Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            orderData.setOrder(lemonJuice);
            lemonJuice.setCookingBehavior(new Heat());
            lemonJuice.performCookingBehavior();
            orderData.setOrder(lemonJuice);



            Console.ReadLine();
        }
    }
}
