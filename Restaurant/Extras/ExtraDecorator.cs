﻿using System;
using Restaurant.Drinks;

namespace Restaurant.Extras
{
    abstract class ExtraDecorator : Drink
    {
        public override abstract String getDescription();
        public override abstract String getPreparation();
    }
}
