﻿using System;
using Restaurant.Drinks;

namespace Restaurant.Extras
{
    class Mocha : ExtraDecorator
    {
        Drink drink;
        public Mocha(Drink drink)
        {
            this.drink = drink;
        }
        public override double getCost()
        {
            return 3 + drink.getCost();
        }

        public override string getDescription()
        {
            return drink.getDescription() + ", Mocka";
        }

        public override string getPreparation()
        {
            return drink.getPreparation();
        }
    }
}
