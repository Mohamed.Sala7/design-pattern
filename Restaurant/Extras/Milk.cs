﻿using System;
using Restaurant.Drinks;

namespace Restaurant.Extras
{
    class Milk : ExtraDecorator
    {
        Drink drink;
        public Milk(Drink drink)
        {
            this.drink = drink;
        }
        public override double getCost()
        {
            return 5 + drink.getCost();
        }

        public override String getDescription()
        {
            return drink.getDescription() + ", Milk";
        }

        public override string getPreparation()
        {
            return drink.getPreparation();
        }
    }
}
