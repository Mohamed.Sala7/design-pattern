﻿using System;
using Restaurant.Drinks;

namespace Restaurant.Observers
{
    interface Observer
    {
        void update(Drink drink);

    }
}
