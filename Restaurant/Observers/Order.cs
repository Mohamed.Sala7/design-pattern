﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Restaurant.Drinks;

namespace Restaurant.Observers
{
    class Order : Subject
    {
        private List<Observer> observers;
        private Drink drink;
        public Order()
        {
            observers = new List<Observer>();
        }

        public void registerObserver(Observer o)
        {
            observers.Add(o);
        }

        public void removeObserver(Observer o)
        {
            observers.Remove(o);
        }
        public void notifyObservers()
        {
            for (int i = 0; i < observers.Count(); i++)
            {
                Observer observer = (Observer)observers.ElementAt(i);
                observer.update(this.drink);
            }
        }

        public void orderCreated()
        {
            notifyObservers();
        }

        public void setOrder(Drink drink)
        {
            this.drink = drink;
            orderCreated();
        }

    }
}
