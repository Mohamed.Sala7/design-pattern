﻿using System;
using Restaurant.Drinks;

namespace Restaurant.Observers
{
    class Accountant : Observer
    {
        private Drink drink;
        private Subject orderData;

        public Accountant(Subject orderData)
        {
            this.orderData = orderData;
            orderData.registerObserver(this);
        }

        public void update(Drink drink)
        {
            this.drink = drink;
            display();
        }

        public void display()
        {
            Console.WriteLine("Accountant => Description: " + this.drink.getDescription()+ ", Cost: " + this.drink.getCost() );
        }

    }
}
