﻿using System;
using Restaurant.Drinks;

namespace Restaurant.Observers
{
    class Kitchen : Observer
    {
        private static Kitchen uniqueInstance;

        private Subject orderData;
        private Drink drink;

        private Kitchen(Subject orderData)
        {
            this.orderData = orderData;
            orderData.registerObserver(this);
        }
        public static Kitchen getInstance(Subject orderData)
        {
            if (uniqueInstance == null)
            {
                uniqueInstance = new Kitchen(orderData);
            }
            return uniqueInstance;
        }



        public void update(Drink drink)
        {
            this.drink = drink;
            display();
        }

        public void display()
        {
            Console.WriteLine("Kitchen => Description: " + this.drink.getDescription() + "  " + this.drink.getPreparation());
        }
    }
}
